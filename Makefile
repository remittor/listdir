CC        = gcc
CFLAGS    = -Wall -g
LDOPTS    = -lpthread
APPOBJS   = listdir.o

prefix    = /usr
bindir    = $(prefix)/bin

all: listdir

ptunnel: $(APPOBJS)
	$(CC) -o $@ $^ $(LDOPTS)

clean:
	-rm -f *.o listdir

%.o:%.cpp
	$(CC) $(CFLAGS) -c -o $@ $<
