#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
       
extern const char * g_progname;

#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif

#ifndef max
#define max(a,b) ((a) < (b) ? (b) : (a))
#endif

#define SIZE_ROUNDUP(_size_,_block_)    (((_size_) + ((_block_)-1)) & ~((_block_)-1))

#define GOTO_EXIT_IF(_cond_,_err_) do { \
  if (_cond_) {                         \
    rc = (_err_);                       \
    goto exit;                          \
  }                                     \
} while(0)

#define FATAL(...) do {                  \
  if (g_progname)                        \
    fprintf(stderr, "%s: ", g_progname); \
  fprintf(stderr, "FATAL ERROR: ");      \
  fprintf(stderr, __VA_ARGS__);          \
  fprintf(stderr, "\n");                 \
  exit(EXIT_FAILURE);                    \
} while(0)

#define FATAL_IF(_cond_, ...) do { if (_cond_) { FATAL(__VA_ARGS__); } } while(0)
  
inline char * xstrdup(const char * s)
{
  char * str = strdup(s);
  FATAL_IF(!str, "not enough memory");
  return str;
}
  
typedef struct list_s {
  struct list_s * next;   /* NULL for the last item in a list */
  int      data;          /* hardlink count */
  struct stat attr;
  char   * mode;          /* type and permissions (text) */
  char   * hardlink;      /* hardlink count (text) */
  char   * user;
  char   * group;
  char   * rdev_major;
  char   * rdev_minor;
  char   * size;
  char   * mtime;
  char   * name;          /* d_name */
  char   * realname;
} list_t;

typedef struct max_len {
  int mode;
  int hardlink;
  int user;
  int group;
  int rdev_major;
  int rdev_minor;
  int size;
  int mtime;
} max_len_t;

#define XFREE(_x_)  do { if (_x_) free(_x_); } while(0)

/* Free item data */
inline void free_list_item(list_t * item)
{
  XFREE(item->mode);
  XFREE(item->hardlink);
  XFREE(item->user);
  XFREE(item->group);
  XFREE(item->rdev_major);
  XFREE(item->rdev_minor);
  XFREE(item->size);
  XFREE(item->mtime);
  XFREE(item->name);
  XFREE(item->realname);
}

/* Counts the number of items in a list. */
inline int count_list_items(const list_t * head)
{
  int count = 0;
  while (head) {
    count++;
    head = head->next;
  }
  return count;
}

/* Inserts a new list item after the one specified as the argument. */
inline list_t * insert_next_to_list(list_t * item, int data)
{
  list_t * newelem = (list_t *) calloc(1, sizeof(list_t));
  if (newelem) {
    newelem->data = data;
    if (item) {
      newelem->next = item->next;
      item->next = newelem;
    }
  }
  return newelem;
}

/* Removes an item following the one specificed as the argument. */
inline void remove_next_from_list(list_t * item)
{
  if (item->next) {
    list_t * tmp = item->next->next;
    free(item->next);
    item->next = tmp;
  }
}

/* Returns item data as text. */
inline char * item_data(const list_t * item)
{
  char buf[12];
  snprintf(buf, sizeof(buf), "%d", item->data);
  return strdup(buf);
}

/* Free all list items */
inline void free_list(list_t * head)
{
  while (head) {
    list_t * current = head;
    free_list_item(current);
    head = current->next;
    free(current);
  }
}

