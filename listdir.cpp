#include "listdir.h"
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <sys/param.h>
#include <stdint.h>
#include <sys/xattr.h>

const char * g_progname = NULL;
list_t * g_head = NULL;
list_t * g_tail = NULL;
uintmax_t g_total_size = 0;
max_len_t g_max = {0};


#define UPDATE_MAX_LEN(_var_) update_max_len(&g_max._var_, item->_var_)

inline int update_max_len(int * plen, const char * str)
{
  int len = (int) strlen(str);
  if (len > *plen)
    *plen = len;
  return *plen;
}

static char get_item_type(mode_t st_mode)
{
  switch (st_mode & S_IFMT) {
  case S_IFLNK:      /* This is a symbolic link. */
    return 'l';
  case S_IFDIR:      /* This is a directory. */
    return 'd';
  case S_IFBLK:      /* This is a block device. */
    return 'b';
  case S_IFCHR:      /* This is a character device. */
    return 'c';
  case S_IFIFO:      /* This is a named pipe (FIFO). */
    return 'f';
  case S_IFSOCK:     /* This is a UNIX domain socket. */
    return 's';
  }
  return '-';        /* This is a regular file and other */
}

#define PERM_UNKNOWN '-'
#define PERM_READ    'r'
#define PERM_WRITE   'w'
#define PERM_EXECUTE 'x'

static int get_type_and_permis(list_t * item, const char * name)
{
  char txt[24];
  char * x = txt;
  mode_t st_mode = item->attr.st_mode;

  *x++ = get_item_type(st_mode);
  
  *x++ = (st_mode & S_IRUSR) ? PERM_READ : PERM_UNKNOWN;
  *x++ = (st_mode & S_IWUSR) ? PERM_WRITE : PERM_UNKNOWN;
  *x++ = (st_mode & S_IXUSR) ? PERM_EXECUTE : PERM_UNKNOWN;

  *x++ = (st_mode & S_IRGRP) ? PERM_READ : PERM_UNKNOWN;
  *x++ = (st_mode & S_IWGRP) ? PERM_WRITE : PERM_UNKNOWN;
  *x++ = (st_mode & S_IXGRP) ? PERM_EXECUTE : PERM_UNKNOWN;

  *x++ = (st_mode & S_IROTH) ? PERM_READ : PERM_UNKNOWN;
  *x++ = (st_mode & S_IWOTH) ? PERM_WRITE : PERM_UNKNOWN;
  if (S_ISDIR(st_mode) && (st_mode & S_ISVTX)) {
    *x++ = 't';   /* sticky bit */
  } else {
    *x++ = (st_mode & S_IXOTH) ? PERM_EXECUTE : PERM_UNKNOWN;
  }

  ssize_t ret = llistxattr(name, NULL, 0);
  if (ret > 0) {
    *x++ = '@';   /* entry have xattr */
  }
  
  *x = 0;
  item->mode = xstrdup(txt);
  UPDATE_MAX_LEN(mode);
  return 0;
}

static int get_hardlink_count(struct stat * attr)
{
  return (int)attr->st_nlink;
}

static int get_user_name(list_t * item)
{
  struct passwd pwd;
  struct passwd * pwd_data;

  size_t bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);
  if (bufsize < 0)            /* Value was indeterminate */
    bufsize = 16384;          /* Should be more than enough */
  char * buf = (char *) malloc(bufsize);
  FATAL_IF(!buf, "not enough memory");

  int rc = getpwuid_r(item->attr.st_uid, &pwd, buf, bufsize, &pwd_data);
  item->user = xstrdup((pwd_data == NULL) ? "" : pwd.pw_name);
  UPDATE_MAX_LEN(user);

  free(buf);
  return 0;
}

static int get_group_name(list_t * item)
{
  struct group grp;
  struct group * grp_data;

  size_t bufsize = sysconf(_SC_GETGR_R_SIZE_MAX);
  if (bufsize < 0)            /* Value was indeterminate */
    bufsize = 16384;          /* Should be more than enough */
  char * buf = (char *) malloc(bufsize);
  FATAL_IF(!buf, "not enough memory");

  int rc = getgrgid_r(item->attr.st_gid, &grp, buf, bufsize, &grp_data);
  item->group = xstrdup((grp_data == NULL) ? "" : grp.gr_name);
  UPDATE_MAX_LEN(group);

  free(buf);
  return 0;
}

static int get_date_time(list_t * item)
{
  int rc = -1;
  char txt[128];
  struct tm t;

  struct tm * pt = localtime_r(&item->attr.st_mtime, &t);
  GOTO_EXIT_IF(!pt, EFAULT);
  rc = snprintf(txt, sizeof(txt), "%04d-%02d-%02d %02d:%02d",
    1900 + t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min);
  GOTO_EXIT_IF(rc <= 0, EFAULT);
  rc = 0;

exit:
  item->mtime = xstrdup((rc != 0) ? "" : txt);
  UPDATE_MAX_LEN(mtime);
  return rc;
}

static int get_file_size(list_t * item)
{
  int rc = -1;
  char txt[64];

  if (S_ISCHR(item->attr.st_mode) || S_ISBLK(item->attr.st_mode)) {
    dev_t rdev = item->attr.st_rdev;

    rc = snprintf(txt, sizeof(txt), "%ju, ", (uintmax_t)major(rdev));
    item->rdev_major = xstrdup((rc <= 0) ? "" : txt);
    UPDATE_MAX_LEN(rdev_major);

    rc = snprintf(txt, sizeof(txt), "%ju", (uintmax_t)minor(rdev));
    item->rdev_minor = xstrdup((rc <= 0) ? "" : txt);
    UPDATE_MAX_LEN(rdev_minor);
  } else {
    rc = snprintf(txt, sizeof(txt), "%ju", (uintmax_t)item->attr.st_size);
    item->size = xstrdup((rc <= 0) ? "" : txt);
    UPDATE_MAX_LEN(size);
  }
  return 0;
}

static int get_file_name(list_t * item, const char * name, struct dirent * de)
{
  int rc = -1;
  char * buf = NULL;
  
  item->name = xstrdup(de->d_name);

  if (S_ISLNK(item->attr.st_mode)) {
    size_t buf_size = 256;
    while (1) {
      buf = (char *) malloc(buf_size);
      FATAL_IF(!buf, "not enough memory");
      ssize_t ret = readlink(name, buf, buf_size-1);
      if (ret >= 0) {
        buf[ret] = 0;
        break;
      }
      free(buf);
      buf = NULL;
      if (errno != ERANGE) {
        item->realname = xstrdup("?");  /* may be show error message ? */
        break;
      }
      if (buf_size >= PATH_MAX) {
        item->realname = xstrdup("?");  /* may be show error message ? */
        break;
      }
      buf_size *= 2;
    }
  }
  if (buf && !item->realname) {
    item->realname = xstrdup(buf);
  }
  rc = 0;
  
  if (buf)
    free(buf);
  return rc;
}

static int process_dir_elem(list_t * item, const char * name,
                            struct dirent * de, struct stat * attr)
{
  item->attr = *attr;

  item->hardlink = item_data(item);
  FATAL_IF(!item->hardlink, "not enough memory");
  UPDATE_MAX_LEN(hardlink);
  
  get_type_and_permis(item, name);
  get_user_name(item);  
  get_group_name(item);
  get_file_size(item);
  get_date_time(item);
  get_file_name(item, name, de);
  
  return 0;
}

static int add_new_dir_elem(const char * dir, struct dirent * de)
{
  int rc = 0;
  char * name = NULL;
  struct stat attr;
  int data;
  list_t * item;

  if (de->d_name[0] == 0)
    return EINVAL;

  if (strcmp(dir, "/") == 0)
    dir = "";
  rc = asprintf(&name, "%s/%s", dir, de->d_name);
  FATAL_IF(rc <= 0, "not enough memory");
  
  rc = lstat(name, &attr);
  FATAL_IF(rc < 0, "can't get attrs for \"%s\" (err = %d)", name, errno);

  g_total_size += SIZE_ROUNDUP(attr.st_size, attr.st_blksize);

  GOTO_EXIT_IF(strcmp(de->d_name, ".") == 0, EINVAL);
  GOTO_EXIT_IF(strcmp(de->d_name, "..") == 0, EINVAL);
  GOTO_EXIT_IF(de->d_name[0] == '.', EINVAL);   /* skip hidden dir entries */

  data = get_hardlink_count(&attr);
  item = insert_next_to_list(g_tail, data);
  GOTO_EXIT_IF(!item, ENOMEM);

  if (process_dir_elem(item, name, de, &attr) < 0) {
    free_list_item(item);
    if (g_tail)
      remove_next_from_list(g_tail);
    GOTO_EXIT_IF(1, EFAULT);
  }

  if (!g_head)
    g_head = item;

  g_tail = item;
  rc = 0;

exit:  
  if (name)
    free(name);
  return rc;
}

static int listdirsort(const struct dirent ** de1, const struct dirent ** de2)
{
  const char * s1 = (*de1)->d_name;
  const char * s2 = (*de2)->d_name;
  if (*s1 == '.')
    s1++;
  if (*s2 == '.')
    s2++;
  return strcasecmp(s1, s2);
}

static void print_padding(size_t len, int max_len)
{
  char txt[128];
  if (len < max_len) {
    char * x = txt;
    len = max_len - len;
    if (len >= sizeof(txt))
      len = sizeof(txt) - 1;
    for (int i=0; i < len; i++) {
      *x++ = ' ';
    }
    *x = 0;
    printf("%s", txt);
  }
}

static void print_pad(const char * str, int max_len)
{
  return print_padding(strlen(str), max_len);
}

static int print_list(void)
{
  int count = 0;

  printf("total %ju\n", g_total_size / 1024); /* always show in KB */

  int size_max = max(g_max.size, g_max.rdev_major + g_max.rdev_minor);
  list_t * item = g_head;
  while (item) {
    count++;
    
    printf("%s", item->mode);
    print_pad(item->mode, g_max.mode);

    print_pad(item->hardlink, g_max.hardlink + 1);
    printf("%s", item->hardlink);

    printf(" %s", item->user);
    print_pad(item->user, g_max.user);

    printf(" %s", item->group);
    print_pad(item->group, g_max.group);

    if (item->rdev_major) {
      size_t len = strlen(item->rdev_major) + g_max.rdev_minor;
      print_padding(len, size_max + 1);
      printf("%s", item->rdev_major);
      print_pad(item->rdev_minor, g_max.rdev_minor);
      printf("%s", item->rdev_minor);
    } else {
      print_pad(item->size, size_max + 1);
      printf("%s", item->size);
    }

    print_pad(item->mtime, g_max.mtime + 1);
    printf("%s", item->mtime);
    
    printf(" %s", item->name);
    
    if (item->realname)
      printf(" -> %s", item->realname);
    
    printf("\n");
    item = item->next;
  }
  return count;
}

int main(int argc, char ** argv)
{
  struct dirent ** namelist = NULL;

  FATAL_IF(argc < 1, "progname not found");
  g_progname = argv[0];
  tzset();

  const char * dir = (argc > 1) ? argv[1] : ".";
  int n = scandir(dir, &namelist, NULL, listdirsort);
  FATAL_IF(n < 0, "can't enum current dir!");

  for (int i=0; i < n; i++) {
    add_new_dir_elem(dir, namelist[i]);
    free(namelist[i]);
  }
  free(namelist);

  print_list();
  
  free_list(g_head);
  return 0;
}
